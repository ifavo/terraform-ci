Example Project for Terraforming git Gitlab CI


* Init for local  with:

```shell
CI_API_V4_URL="https://gitlab.com/api/v4" CI_PROJECT_ID=<Project_Id> GITLAB_USER_LOGIN="<Username>" GITLAB_ACCESS_TOKEN="<Access_Token>" CI_PROJECT_NAME="test" GITLAB_TF_ADDRESS="${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/terraform/state/${CI_PROJECT_NAME}"; terraform init \
    -backend-config="address=${GITLAB_TF_ADDRESS}" \
    -backend-config="lock_address=${GITLAB_TF_ADDRESS}/lock" \
    -backend-config="unlock_address=${GITLAB_TF_ADDRESS}/lock" \
    -backend-config="username=${GITLAB_USER_LOGIN}" \
    -backend-config="password=${GITLAB_ACCESS_TOKEN}" \
    -backend-config="lock_method=POST" \
    -backend-config="unlock_method=DELETE" \
    -backend-config="retry_wait_min=5"
```