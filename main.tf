provider "digitalocean" {
}

resource "digitalocean_database_cluster" "postgres-shared" {
  name       = "test-postgres-shared"
  engine     = "pg"
  version    = "12"
  size       = "db-s-1vcpu-1gb"
  region     = "fra1"
  node_count = 1
}

resource "digitalocean_project" "my-project" {
  name        = "Test"
  description = "Test Resources"
  purpose     = "Web Application"
  environment = "Development"
  resources   = [digitalocean_database_cluster.postgres-shared.urn]
}

output "Database" {
  value = {
    "Shared" = digitalocean_database_cluster.postgres-shared.uri
  }
}
